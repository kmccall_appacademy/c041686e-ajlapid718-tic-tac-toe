class HumanPlayer
	attr_reader :name
	attr_accessor :mark

	def initialize(name)
		@name = name
		@mark = mark
	end

	def get_move
		print "Where would you like to move? (ex. 0, 1): "
		move = gets.chomp
		move.split(",").map(&:to_i)
	end

	def display(board)
		@board = board
		puts
		puts board.to_s
	end
end
